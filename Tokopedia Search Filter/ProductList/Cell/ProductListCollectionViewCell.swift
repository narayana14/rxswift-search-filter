//
//  ProductListCollectionViewCell.swift
//  Tokopedia Search Filter
//
//  Created by Narayana Wijaya on 09/11/19.
//  Copyright © 2019 Nara Jaya. All rights reserved.
//

import UIKit
import SDWebImage

struct ProductListCellData {
    let imageUrl: URL
    let name: String
    let price: String
}

class ProductListCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "ProductCell"
    
    private let imageView = UIImageView()
    private let nameLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 2
        return lbl
    }()
    
    private let priceLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .red
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        contentView.backgroundColor = .white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(imageView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(priceLabel)
        
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8),
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor),
            
            nameLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            nameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8),
            nameLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 8),
//            nameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 8)
            
            priceLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            priceLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8),
            priceLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8),
            priceLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])
    }
    
    func configure(with data: ProductListCellData) {
        nameLabel.text = data.name
        priceLabel.text = data.price
        imageView.sd_setImage(with: data.imageUrl, placeholderImage: UIImage(named: "Tokopedia_Mascot"), options: .refreshCached, completed: nil)
    }
}
