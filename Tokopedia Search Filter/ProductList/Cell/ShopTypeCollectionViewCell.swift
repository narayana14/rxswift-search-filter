//
//  ShopTypeCollectionViewCell.swift
//  Mini Project - Search Filter
//
//  Created by Narayana Wijaya on 28/09/18.
//  Copyright © 2018 Narayana Wijaya. All rights reserved.
//

import UIKit

class ShopTypeCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier = "ShopTypeCollectionViewCell"
    
    private let bgView: UIView = {
        let bgView = UIView()
        bgView.translatesAutoresizingMaskIntoConstraints = false
        bgView.layer.borderColor = UIColor.gray.cgColor
        bgView.layer.borderWidth = 0.5
        bgView.layer.cornerRadius = bgView.bounds.height/2
        bgView.layer.masksToBounds = true
        
        return bgView
    }()
    
    private let nameLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let deleteButton: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(.remove, for: .normal)
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = btn.bounds.height/2
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        bgView.addSubview(nameLabel)
        bgView.addSubview(deleteButton)
        contentView.addSubview(bgView)
        
        bgView.frame = contentView.frame
        bgView.center = contentView.center
        
        NSLayoutConstraint.activate([
//            bgView.heightAnchor.constraint(equalTo: contentView.heightAnchor),
//            bgView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
//            bgView.center.constraint(equalTo: contentView.heightAnchor),
        ])
    }
    
    func configure(title: String) {
        self.nameLabel.text = title
    }
}
