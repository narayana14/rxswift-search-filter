//
//  ProductListViewModel.swift
//  Tokopedia Search Filter
//
//  Created by Narayana Wijaya on 10/11/19.
//  Copyright © 2019 Nara Jaya. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

internal class ProductListViewModel: ViewModelType {
    let service: ProductServiceProtocol
    
    init(service: ProductServiceProtocol = ProductService()) {
        self.service = service
    }
    
    internal struct Input {
        let viewDidLoad: Driver<ProductsFilterData>
        let loadMore: Driver<CGPoint>
    }
    
    internal struct Output {
        let gridData: Driver<[ProductListCellData]>
        let error: Driver<String>
    }
    
    func transform(input: Input) -> Output {
        let fetchProductListErrorDetail = PublishSubject<String>()
        
        let outputProductListCellData: Driver<[ProductListCellData]>
        
        var start = 0
        
        let fetchProductList = input.viewDidLoad
            .asObservable()
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            .flatMapLatest { [service] param -> Observable<[Product]> in
                return service
                    .fetchData(pmin: param.pmin, pmax: param.pmax, wholesale: param.isWholeSale, official: param.isOfficial, fshop: param.fShop, start: start)
                    .do(onError: { err in
                        fetchProductListErrorDetail.onNext(err.localizedDescription)
                    })
        }
        
        let productListCellData = fetchProductList.map { (products) -> [ProductListCellData] in
            return products.map { (product) -> ProductListCellData in
                return ProductListCellData(imageUrl: URL(string: product.imageUri)!, name: product.name, price: product.price)
            }
        }
        
//        let loadMoreProduct = input.loadMore
//            .asObservable()
//            .observeOn(ConcurrentDispatchQueueScheduler(qos: .userInitiated))
//            .flatMapLatest { [service] param -> Observable<[Product]> in
//                start = start + 1
//                return service
//                    .fetchData(pmin: 1000, pmax: 100000000, wholesale: false, official: false, fshop: 2, start: start)
//                    .do(onError: { err in
//                            fetchProductListErrorDetail.onNext(err.localizedDescription)
//                    })
//        }

//        let productListMoreCellData = loadMoreProduct.map { (products) -> [ProductListCellData] in
//            return products.map { (product) -> ProductListCellData in
//                return ProductListCellData(imageUrl: URL(string: product.imageUri)!, name: product.name, price: product.price)
//            }
//        }
        
        outputProductListCellData = productListCellData.asDriver(onErrorJustReturn: [])
        
        let outputError = fetchProductListErrorDetail
            .asDriver(onErrorJustReturn: "")
            .filter { !$0.isEmpty }
        
        return Output(gridData: outputProductListCellData, error: outputError)
    }
}
