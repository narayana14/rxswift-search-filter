//
//  Product.swift
//  Tokopedia Search Filter
//
//  Created by Narayana Wijaya on 10/11/19.
//  Copyright © 2019 Nara Jaya. All rights reserved.
//

import Foundation

struct Product: Decodable {
    let name: String
    let imageUri: String
    let price: String
}

struct ProductListResponse: Decodable {
    let data: [Product]
}
