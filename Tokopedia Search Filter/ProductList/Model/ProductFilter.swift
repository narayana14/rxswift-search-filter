//
//  ProductFilter.swift
//  Tokopedia Search Filter
//
//  Created by Narayana Wijaya on 17/11/19.
//  Copyright © 2019 Nara Jaya. All rights reserved.
//

import Foundation

class ProductsFilterData {
    var pmin: Int = 0
    var pmax: Int = 10000000
    var isWholeSale: Bool = false
    var isOfficial: Bool = false
    var fShop: Int = 2
    
    init() {}
    
    init(pmin: Int, pmax: Int, isWholeSale: Bool, isOfficial: Bool, fShop: Int) {
        self.pmin = pmin
        self.pmax = pmax
        self.isWholeSale = isWholeSale
        self.isOfficial = isOfficial
        self.fShop = fShop
    }
    
    func resetFilter() {
        pmin = 0
        pmax = 10000000
        isWholeSale = false
        isOfficial = false
        fShop = 2
    }
}
