//
//  ProductService.swift
//  Tokopedia Search Filter
//
//  Created by Narayana Wijaya on 10/11/19.
//  Copyright © 2019 Nara Jaya. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol ProductServiceProtocol {
    func fetchData(pmin: Int, pmax: Int, wholesale: Bool, official: Bool, fshop: Int, start: Int) -> Observable<[Product]>
}

class ProductService: ProductServiceProtocol {
    func fetchData(pmin: Int, pmax: Int, wholesale: Bool, official: Bool, fshop: Int, start: Int) -> Observable<[Product]> {
        let url = URL(string: "https://ace.tokopedia.com/search/v2.5/product?q=samsung&pmin=\(pmin)&pmax=\(pmax)&wholesale=\(wholesale)&official=\(official)&fshop=\(fshop)&start=\(start)&rows=10")!
        
        let urlRequest = URLRequest(url: url)
        
        let response = URLSession.shared.rx
            .data(request: urlRequest)
            .flatMapLatest { (data) -> Observable<ProductListResponse> in
                print(data)
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                
                do{
                    let resultData = try decoder.decode(ProductListResponse.self, from: data)
                    return Observable.just(resultData)
                } catch (let decodeError) {
                    return Observable.error(decodeError)
                }
        }
        
        let product = response.map{ $0.data }
        return product
    }
}
