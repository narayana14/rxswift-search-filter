//
//  ProductListViewController.swift
//  Tokopedia Search Filter
//
//  Created by Narayana Wijaya on 09/11/19.
//  Copyright © 2019 Nara Jaya. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProductListViewController: UIViewController {

    internal init() {
        self.viewModel = ProductListViewModel()
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init coder has not been implemented")
    }

    private lazy var collectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 2
        layout.minimumLineSpacing = 2
        let width = (view.frame.width - 16 - 2)/2
        layout.itemSize = CGSize(width: width, height: width*4/3)
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        cv.register(ProductListCollectionViewCell.self, forCellWithReuseIdentifier: "ProductCell")
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .clear
        return cv
    }()
    
    private let filterButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("Filter", for: .normal)
        btn.backgroundColor = .systemGreen
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    private let viewModel: ProductListViewModel
    private let disposeBag = DisposeBag()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setupViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        title = "Search"
        view.backgroundColor = .systemGroupedBackground
        
        view.addSubview(collectionView)
        view.addSubview(filterButton)
        
        NSLayoutConstraint.activate([
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8),
            collectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
//            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            filterButton.heightAnchor.constraint(equalToConstant: 60),
            filterButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8),
            filterButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            filterButton.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: 20),
            filterButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8),
        ])
        
        filterButton.addTarget(self, action: #selector(openFilter), for: .touchUpInside)
    }
    
    @objc private func openFilter() {
        let vc = FilterViewController()
        vc.onApplyFilter = { [weak self] data in
            self?.filterData = data
        }
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    var filterData: ProductsFilterData = ProductsFilterData()
    
    private func setupViewModel() {
        let input = ProductListViewModel.Input(
            viewDidLoad: .just(filterData),
            loadMore: collectionView.rx.contentOffset.asDriver()
        )
        
        let output = viewModel.transform(input: input)
        
        output.gridData.drive(
            collectionView.rx.items(
                cellIdentifier: ProductListCollectionViewCell.reuseIdentifier,
                cellType: ProductListCollectionViewCell.self
            )
        ) {_, data, cell in
            cell.configure(with: data)
        }.disposed(by: disposeBag)
        
        
    }
}
