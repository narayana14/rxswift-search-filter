//
//  FilterViewController.swift
//  Tokopedia Search Filter
//
//  Created by Narayana Wijaya on 16/11/19.
//  Copyright © 2019 Nara Jaya. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RangeSeekSlider
import MultiSlider

class FilterViewController: UIViewController {

    internal init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init coder has not been implemented")
    }
    
    var onApplyFilter: ((ProductsFilterData) -> Void)?
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupViewModel()
    }
    
    private func setupViewModel() {
        slider.rx
            .controlEvent(UIControl.Event.valueChanged)
            .subscribe(onNext: { [unowned self] in
                self.minPriceLabel.text = "Rp. \(self.slider.minValue.hashValue.formatCurrency)"
                self.maxPriceLabel.text = "Rp. \(self.slider.maxValue.hashValue.formatCurrency)"
            }).disposed(by: disposeBag)
        
        applyButton.rx
            .controlEvent(UIControl.Event.touchUpInside)
            .subscribe(onNext: { [unowned self] in
                self.onApplyFilter?(ProductsFilterData(
                    pmin: 100000,
                    pmax: 1000000,
                    isWholeSale: self.wholeSaleSwitch.isOn,
                    isOfficial: false,
                    fShop: 2
                ))
                self.dismiss(animated: true, completion: nil)
            }).disposed(by: disposeBag)
    }
    
    private func setupView() {
        view.backgroundColor = .systemGroupedBackground
        shopTypeView.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(resetButton)
        
        priceFilterView.addSubview(minLabel)
        priceFilterView.addSubview(maxLabel)
        priceFilterView.addSubview(minPriceLabel)
        priceFilterView.addSubview(maxPriceLabel)
        priceFilterView.addSubview(divider)
        priceFilterView.addSubview(slider)
        priceFilterView.addSubview(wholeSaleLabel)
        priceFilterView.addSubview(wholeSaleSwitch)
        
        shopTypeView.addSubview(shopTypeLabel)
        shopTypeView.addSubview(shopTypeSelectButton)
        shopTypeView.addSubview(shopSelectionView)
        
        view.addSubview(topView)
        view.addSubview(priceFilterView)
        view.addSubview(shopTypeView)
        view.addSubview(applyButton)
        
        NSLayoutConstraint.activate([
            topView.heightAnchor.constraint(equalToConstant: 60),
            topView.topAnchor.constraint(equalTo: view.topAnchor),
            topView.leftAnchor.constraint(equalTo: view.leftAnchor),
            topView.rightAnchor.constraint(equalTo: view.rightAnchor),
            
            resetButton.centerYAnchor.constraint(equalTo: topView.centerYAnchor),
            resetButton.rightAnchor.constraint(equalTo: topView.rightAnchor, constant: -20),
            
            priceFilterView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 20),
            priceFilterView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 12),
            priceFilterView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            minLabel.topAnchor.constraint(equalTo: priceFilterView.topAnchor, constant: 16),
            minLabel.leftAnchor.constraint(equalTo: priceFilterView.leftAnchor, constant: 8),
            
            minPriceLabel.topAnchor.constraint(equalTo: minLabel.bottomAnchor, constant: 8),
            minPriceLabel.leftAnchor.constraint(equalTo: priceFilterView.leftAnchor, constant: 8),
            
            maxLabel.centerYAnchor.constraint(equalTo: minLabel.centerYAnchor),
            maxLabel.rightAnchor.constraint(equalTo: priceFilterView.rightAnchor, constant: -8),
            
            maxPriceLabel.centerYAnchor.constraint(equalTo: minPriceLabel.centerYAnchor),
            maxPriceLabel.rightAnchor.constraint(equalTo: priceFilterView.rightAnchor, constant: -8),
            
            divider.topAnchor.constraint(equalTo: minPriceLabel.bottomAnchor, constant: 8),
            divider.leftAnchor.constraint(equalTo: priceFilterView.leftAnchor, constant: 8),
            divider.centerXAnchor.constraint(equalTo: priceFilterView.centerXAnchor),
            divider.heightAnchor.constraint(equalToConstant: 1),
            
            slider.topAnchor.constraint(equalTo: divider.bottomAnchor, constant: 8),
            slider.leftAnchor.constraint(equalTo: priceFilterView.leftAnchor, constant: 8),
            slider.centerXAnchor.constraint(equalTo: priceFilterView.centerXAnchor),
            slider.heightAnchor.constraint(equalToConstant: 100),
            
            wholeSaleLabel.topAnchor.constraint(equalTo: slider.bottomAnchor, constant: 8),
            wholeSaleLabel.leftAnchor.constraint(equalTo: priceFilterView.leftAnchor, constant: 8),
            
            wholeSaleSwitch.centerYAnchor.constraint(equalTo: wholeSaleLabel.centerYAnchor),
            wholeSaleSwitch.rightAnchor.constraint(equalTo: priceFilterView.rightAnchor, constant: -8),
            
            wholeSaleSwitch.bottomAnchor.constraint(equalTo: priceFilterView.bottomAnchor, constant: -16),
            
            applyButton.heightAnchor.constraint(equalToConstant: 60),
            applyButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8),
            applyButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            applyButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8),
            
            shopTypeView.topAnchor.constraint(equalTo: priceFilterView.bottomAnchor, constant: 20),
            shopTypeView.leftAnchor.constraint(equalTo: priceFilterView.leftAnchor),
            shopTypeView.centerXAnchor.constraint(equalTo: priceFilterView.centerXAnchor),
            
            shopTypeLabel.topAnchor.constraint(equalTo: shopTypeView.topAnchor, constant: 16),
            shopTypeLabel.leftAnchor.constraint(equalTo: shopTypeView.leftAnchor, constant: 8),
            
            shopTypeSelectButton.centerYAnchor.constraint(equalTo: shopTypeLabel.centerYAnchor),
            shopTypeSelectButton.rightAnchor.constraint(equalTo: shopTypeView.rightAnchor, constant: -8),
            
            shopSelectionView.topAnchor.constraint(equalTo: shopTypeLabel.topAnchor, constant: 8),
            shopSelectionView.centerXAnchor.constraint(equalTo: shopTypeView.centerXAnchor),
            shopSelectionView.heightAnchor.constraint(equalToConstant: 100),
            
            shopSelectionView.bottomAnchor.constraint(equalTo: shopTypeView.bottomAnchor, constant: -16)
        ])
    }
    
    private let resetButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("Reset", for: .normal)
        btn.setTitleColor(.systemGreen, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    private let topView: UIView = {
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        return vw
    }()
    
    private let minLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Minimum Price"
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = .lightGray
        return lbl
    }()
    
    private let minPriceLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "1000"
        lbl.textColor = .darkGray
        return lbl
    }()
    
    private let maxLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Maximum Price"
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = .lightGray
        return lbl
    }()
    
    private let maxPriceLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "100000000"
        lbl.textColor = .darkGray
        return lbl
    }()
    
    private let priceFilterView: UIView = {
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        return vw
    }()
    
    private let divider: UIView = {
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .systemGroupedBackground
        
        return vw
    }()
    
//    private let slider: MultiSlider = {
//        let sld = MultiSlider()
//        sld.translatesAutoresizingMaskIntoConstraints = false
////        sld.minimumValue = 0
////        sld.maximumValue = 100
//        sld.tintColor = .systemGreen
//        sld.outerTrackColor = .systemGray
//        sld.orientation = .horizontal
//        sld.hasRoundTrackEnds = true
//        sld.thumbImage   = UIImage(named: "balloon")
//
//        return sld
//    }()
    
    private let slider: RangeSeekSlider = {
        let slider = RangeSeekSlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.hideLabels = true
//        slider.minDistance = 20
//        slider.minValue = 0
//        slider.maxValue = 10000000
//        slider.step = 20

        slider.tintColor = .systemGray
        slider.lineHeight = 4
        slider.colorBetweenHandles = .systemGreen

        slider.handleColor = .white
        slider.handleBorderColor = .systemGreen
        slider.handleBorderWidth = 1
        slider.handleDiameter = 30
        slider.selectedHandleDiameterMultiplier = 1

        return slider
    }()
    
//    private let slider: RangeSlider = {
//        let slider = RangeSlider()
//        slider.minimumValue = 100
//        slider.maximumValue = 10000000
//        slider.translatesAutoresizingMaskIntoConstraints = false
//
//        slider.tintColor = .systemGreen
//        slider.knobSize = 10
//        slider.knobBorderTintColor = .systemGreen
//        slider.backgroundColor = .yellow
//        slider.trackTintColor = .systemGreen
//
//        return slider
//    }()
    
    private let wholeSaleLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Whole Sale"
        lbl.textColor = .lightGray
        return lbl
    }()
    
    private let wholeSaleSwitch: UISwitch = {
        let swt = UISwitch()
        swt.translatesAutoresizingMaskIntoConstraints = false
        return swt
    }()
    
    private let shopTypeView: UIView = {
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        return vw
    }()
    
    private let shopTypeLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Shop Type"
        lbl.textColor = .lightGray
        return lbl
    }()
    
    private let shopTypeSelectButton: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "icons8-more-than-green"), for: .normal)
        
        return btn
    }()
    
    private let applyButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("Apply", for: .normal)
        btn.backgroundColor = .systemGreen
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    private lazy var shopSelectionView: UICollectionView = {
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(ShopTypeCollectionViewCell.self, forCellWithReuseIdentifier: "ShopTypeCollectionViewCell")
        cv.backgroundColor = .yellow
        return cv
    }()
}
