//
//  NumberExtension.swift
//  Tokopedia Search Filter
//
//  Created by Narayana Wijaya on 17/11/19.
//  Copyright © 2019 Nara Jaya. All rights reserved.
//

import Foundation

extension Int {
    var formatCurrency: String {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "."
        formatter.numberStyle = .currency
        return formatter.string(for: self) ?? "0"
    }
}
