//
//  ViewModelType.swift
//  Tokopedia Search Filter
//
//  Created by Narayana Wijaya on 09/11/19.
//  Copyright © 2019 Nara Jaya. All rights reserved.
//

import Foundation

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    func transform(input: Input) -> Output
}
